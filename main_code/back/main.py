import radio.radio_class as radio
import threading
import socket
import time

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = ('localhost', 1223)
print('starting up on {} port {}'.format(*server_address))
sock.bind(server_address)

def do_chk(radio):
	global end
	while 1:
		try:
			radio.internet_connect()
			radio.audio_device()
		except KeyboardInterrupt:
			end = True
		except Exception as e:
			print(e)
		if end == True:
			break
		time.sleep(1)

def listeners(radio):
	global end
	while 1:
		try:
			radio.n_listeners()
		except KeyboardInterrupt:
			end = True
		except Exception as e:
			print(e)
		if end == True:
			break
		time.sleep(1)


if __name__ == '__main__':
	global end
	try:
		end = False
		r_tm = radio.radio_class()
		d_chk = threading.Thread(target = do_chk, args = (r_tm,))
		lis = threading.Thread(target = listeners, args = (r_tm,))
		d_chk.start()
		lis.start()
		while 1:
			data, address = sock.recvfrom(4096)
			data = data.decode().split(',')
			act = data[0]
			val = data[1]
			# Transmitir
			if act == 't':
				if val == '1':
					print("iniciar - transmitir")
					r_tm.stream_up()
				if val == '0':
					print("parar - transmitir")
					r_tm.stream_down()
			if act == 'g':
				if val == '1':
					print("iniciar - grabar")
					r_tm.grabar_up()
				if val == '0':
					print("parar - grabar")
					r_tm.grabar_down()

	except KeyboardInterrupt:
		end = True
		print('Fin del Programa')
