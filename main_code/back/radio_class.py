import pymysql
import os
import subprocess as sub
import time
import configparser
from urllib.request import urlopen

class radio_class:
	link 		= ''	# Broadcast Link
	nombre 		= ''	# Broadcast Name
	ubicacion 	= ''	# Broadcast Location
	descripcion	= ''	# Broadcast Description
	genero		= '' 	# Broadcast genre
	mount_Point	= ''

	def __init__(self):
		# Get Mount Point
		self.gen_mountPoint()

	def gen_mountPoint(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		proc = sub.Popen('cat /sys/class/net/eth0/address', shell=True, stdout=sub.PIPE, )
		phy_add=proc.communicate()[0]
		add_phy = phy_add[::-1]
		add_phy = add_phy.decode().split('\n')
		self.mount_Point = add_phy[1].replace(':','')
		self.link = 'mensajito.mx/' + self.mount_Point
		sql = "UPDATE transmision set link = '%s' where id=1" %(self.link)
		cursor.execute(sql)
		sql = "UPDATE transmision set mount_Point = '%s' where id=1" %(self.mount_Point)
		cursor.execute(sql)
		db.commit()
		db.close()

	def get_data(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		sql = "SELECT estacion FROM transmision WHERE id=1"
		cursor.execute(sql)
		res = cursor.fetchall()
		db.commit()
		db.close()
		self.nombre = res[0][0]

	def cfg_darkice(self):
		os.system('sudo rm /etc/darkice.cfg')
		Config = configparser.ConfigParser()
		Config.optionxform = str
		cfgfile = open("/home/pi/ctrl_radio/aux_cfg",'w')
		Config.add_section('general')
		Config.add_section('input')
		Config.add_section('icecast2-0')
		# [general]
		Config.set('general', 'duration', '0')
		Config.set('general', 'bufferSecs', '60')
		Config.set('general', 'reconnect', 'yes')
		# [input]
		Config.set('input', 'device', 'mensajito')
		Config.set('input', 'sampleRate', '48000')
		Config.set('input', 'bitsPerSample', '16')
		Config.set('input', 'channel', '2')
		# [icecast2-0]
		Config.set('icecast2-0', 'bitrateMode', 'vbr')
		Config.set('icecast2-0', 'bitrate', '128')
		Config.set('icecast2-0', 'format', 'mp3')
		Config.set('icecast2-0', 'quality', '0.6')
		Config.set('icecast2-0', 'server', 'mensajito.mx')
		Config.set('icecast2-0', 'port','8000')
		Config.set('icecast2-0', 'password', 'mensajito$1192')
		Config.set('icecast2-0', 'mountPoint', self.mount_Point)
		Config.set('icecast2-0', 'sampleRate', '48000')
		Config.set('icecast2-0', 'channel', '2')
		Config.set('icecast2-0', 'name', self.nombre)
		Config.set('icecast2-0', 'description', self.descripcion)
		Config.set('icecast2-0', 'genre', self.genero)
		Config.set('icecast2-0', 'public', 'yes')
		# Write the changes
		Config.write(cfgfile)
		cfgfile.close()
		os.system("sudo cp /home/pi/ctrl_radio/aux_cfg /etc/darkice.cfg")
		os.system("sudo rm /home/pi/ctrl_radio/aux_cfg")

	def internet_connect(self):
		internet = False
		count_fail = 0
		for i in range(0, 15):
			ping = os.system('sudo timeout 0.25 ping -c1 8.8.8.8 > /dev/null')
			nb_ping = not(bool(ping))
			if ping == 0:
				count_fail = 0
				self.internet_up()
				internet = True
			else:
				count_fail = count_fail + 1
				if count_fail >= 10:
					self.internet_down()
					internet = False
		return internet

	def internet_up(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		sql = "UPDATE datos set internet = '1' where id=1"
		cursor.execute(sql)
		db.commit()
		db.close()

	def internet_down(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		sql = "UPDATE datos set internet = '0' where id=1"
		cursor.execute(sql)
		db.commit()
		db.close()

	def audio_device(self):
		a_dev = os.popen('arecord -l')
		line_t = a_dev.read()
		line = line_t.split('\n')
		n_lines = len(line)
		audio = False
		if n_lines == 2:
			self.audio_down()
			audio = False
		elif n_lines == 5:
			self.audio_up()
			audio = True
		return audio

	def audio_up(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		sql = "UPDATE datos set audio = '1' where id=1"
		cursor.execute(sql)
		db.commit()
		db.close()

	def audio_down(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		sql = "UPDATE datos set audio = '0' where id=1"
		cursor.execute(sql)
		db.commit()
		db.close()

	def usb_connect(self):
		drive_list = os.listdir('/media/pi/.' )
		if len(drive_list) == 0:
			self.usb_down()
		else:
			self.usb_up()

	def usb_up(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		sql = "UPDATE datos SET usb = '1' WHERE id=1"
		cursor.execute(sql)
		db.commit()
		db.close()

	def usb_down(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		sql = "UPDATE datos SET usb = '0' WHERE id=1"
		cursor.execute(sql)
		db.commit()
		db.close()

	def list_files(self):
		drive_list = os.listdir('/media/pi/.' )
		if len(drive_list) > 0:
			drive_name = drive_list[0]
			imgs = glob.glob('/media/pi/'+drive_name+'/*.png')
			for img in imgs:
				n_img = img.split('/')[4]
				print(n_img)

	def grabar_up(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		sql = "SELECT programa FROM transmision WHERE id=1"
		cursor.execute(sql)
		res = cursor.fetchall()
		db.commit()
		db.close()
		programa = res[0][0].replace(" ", "_")
		date = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())
		nombre_mp3 = "/home/pi/audio/" + programa + "_" + date + ".mp3"
		cmd = 'arecord -f dat --device="mensajito" - | lame - ' + nombre_mp3 + " &"
		os.system(cmd)

	def grabar_down(self):
		cmd = "sudo killall arecord"
		os.system(cmd)

	def stream_up(self):
		self.get_data()
		self.cfg_darkice()
		os.system("darkice &")

	def stream_down(self):
		cmd = "sudo killall darkice"
		os.system(cmd)

	def n_listeners(self):
		db = pymysql.connect("localhost","mensajito_admin","mensajito1992","mensajito_datos")
		cursor = db.cursor()
		try:
			url = 'http://mensajito.mx:8000/' + self.mount_Point + '.xspf'
			f = urlopen(url)
			data = f.read()
			data_split = data.decode().split('\n')
			aux_listen = data_split[12].split(':')
			self.listeners = int(aux_listen[1])
		except:
			self.listeners = 0
		sql = "UPDATE transmision set escuchas = '" + str(self.listeners)  + "' where id=1"
		cursor.execute(sql)
		db.commit()
		db.close()
		return self.listeners
