window.onload = function(){ 

	$.getJSON("php/get_transmision.php", function( data ) {
		console.log(data);
		console.log(data.programa);
		$("#programa_1").text(data.programa);
		$("#ubicacion").text("Transmitiendo desde " + data.ubicacion);
		$("#estacion").text("por " + data.estacion);
	});

	setInterval(get_listeners, 1000);

	$('#transmitiendo_1').hide();
	$('#grabando_1').hide();
	$('.contador').hide();

	$('#transmitir_1').click(function(){
		$('.contador').show();
		$.getJSON("php/get_datos.php", function( data ) {
			var internet = data.internet;
			var audio = data.audio;
			if(audio == '0') {
				console.log('sin audio');
				window.location.href = 'error_audio.html';
			}
			else if (internet == '0') {
				console.log('sin internet');
				window.location.href = 'error_internet.html';
			}
			else {
				$.get("php/send_udp.php", {msg:"t,1"});
				$('#transmitir_1').hide();
				$('#transmitiendo_1').show();
			}
		});
	});

	$('#grabar_1').click(function(){
		$.getJSON( "php/get_datos.php", function( data ) {
			var audio = data.audio;
			if(audio == '0') {
				console.log('sin audio');
				window.location.href = 'error_audio.html';
			}
			else {
				$('#grabar_1').hide();
				$('#grabando_1').show();
				$.get("php/send_udp.php", {msg:"g,1"});
			}
		});
	});

	$('#transmitiendo_1').click(function(){
		$('.contador').hide();
		$('#transmitiendo_1').hide();
		$('#transmitir_1').show();
		$.get("php/send_udp.php", {msg:"t,0"});
	});

	$('#grabando_1').click(function(){
		$('#grabando_1').hide();
		$('#grabar_1').show();
		$.get("php/send_udp.php", {msg:"g,0"});
	});
};

function get_listeners() {
	$.getJSON( "php/get_listeners.php", function( data ) {
		var escuchas = data.escuchas;
		console.log(escuchas);
		$('#contador_1').text(escuchas);
	});
}