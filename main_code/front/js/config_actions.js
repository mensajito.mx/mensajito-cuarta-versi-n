window.onload = function(){ 
	$.getJSON("php/get_info.php", function( data ) {
		$("#estacion").val(data.estacion);
		$("#ubicacion").val(data.ubicacion);
		$("#descripcion").val(data.descripcion);
	});

	$('#archivo_1').click(function(){
		window.location.href='archivo.html';
	});

	$('#config_1').click(function(){
		window.location.href='config.html';
	});

	$('#wi_fi_1').click(function(){
		window.location.href='wi_fi.html';
	});

	$('#info_1').click(function(){
		window.location.href='info.html';
	});

	$('#modificar_1').click(function(){
		var est = $("#estacion").val();
		var ubi = $("#ubicacion").val();
		var des = $("#descripcion").val();
		$.get("php/mod_datos.php", {est: est, ubi: ubi, des: des}, function(data) {
			console.log(data);
		});
		window.location.href='index.html';
	});

	$('#imagen_1').click(function(){
		$.getJSON( "php/get_datos.php", function( data ) {
			var audio = data.usb;
			if(audio == '0') {
				console.log('sin usb');
				//window.location.href = 'error_audio.html';
			}
			else {
				console.log('con usb');
			}
		});
	});
};