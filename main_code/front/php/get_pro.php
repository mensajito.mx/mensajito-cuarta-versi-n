<?php
	$link = mysqli_connect("localhost", "mensajito_admin", "mensajito1992");
	mysqli_select_db($link, "mensajito_datos");
	$tildes = $link->query("SET NAMES 'utf8'"); //Para que se muestren las tildes

	$result = mysqli_query($link, "SELECT nombre FROM programas");

	$pro_arr = array();

	while ($row = mysqli_fetch_array($result)) {
		$programa = $row['nombre'];

		$pro_arr[] = array("programa" => $programa);
	}
	echo json_encode($pro_arr);
	
	mysqli_free_result($result);
	mysqli_close($link);
?>