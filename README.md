mensajito.mx
              

Mensajito, surge de la idea de crear puentes de comunicación libre, una opción económica para quienes quieran desarrollar una estación de radio por internet o una segunda línea de transmisión para quienes ya hacen radio por frecuencia.
              
              

Es gratuito, libre y modificable. busca generar comunidad a partir de contenidos auditivos dando la posibilidad de interconectar personas e intercambiar información, así como generar espacios libres de reflexión sobre tecnología.
              
              

Mensajito es una colaboración creativa sin fines de lucro que busca crear oportunidades para comunidades de artistas, productores musicales, radios comunitarias o personas que quieran experimentar con el audio.
